from selenium import webdriver
import math #Добавьте в самый верх своего кода import math

#Откройте в скрипте страницу: http://suninjuly.github.io/find_link_text
link = " http://suninjuly.github.io/find_link_text"
browser = webdriver.Chrome()
browser.get(link)

#С помощью метода поиска ссылки отыщите нужную.
#Текст ссылки, который нужно найти, зашифрован формулой:
#str(math.ceil(math.pow(math.pi, math.e)*10000))
link = browser.find_element_by_link_text(str(math.ceil(math.pow(math.pi, math.e)*10000)))

#Нажмите на ссылку с найденным ранее текстом: она перенесет вас на форму регистрации
link.click()

#Заполните форму так же как вы делали в предыдущем шаге урока
input1 = browser.find_element_by_tag_name('input')
input1.send_keys("Ivan")
input2 = browser.find_element_by_name('last_name')
input2.send_keys("Petrov")
input3 = browser.find_element_by_class_name('city')
input3.send_keys("Smolensk")
input4 = browser.find_element_by_id('country')
input4.send_keys("Russia")
button = browser.find_element_by_css_selector("button.btn")
button.click()
