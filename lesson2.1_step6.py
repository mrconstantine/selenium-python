from selenium import webdriver
import math

link = "http://suninjuly.github.io/get_attribute.html"
browser = webdriver.Chrome()
browser.get(link)


# Функция для расчета формулы на странице
def calc(x):
    return str(math.log(abs(12 * math.sin(int(x)))))


# Поиск формулы на странице
x_element = browser.find_element_by_xpath('//img[@id="treasure"]')
x_element = x_element.get_attribute("valuex")              # Find value
y = calc(x_element)

# Вставка ответа в поле ввода
input1 = browser.find_element_by_xpath('//input[@id="answer"]')
input1.send_keys(y)

# using checkbox
checkbox = browser.find_element_by_xpath("//input[@id='robotCheckbox']")
checkbox.click()

# using radiobutton
radiobutton = browser.find_element_by_xpath("//input[@id='robotsRule']")
radiobutton.click()

buttonchecked = browser.find_element_by_xpath("//button[@class='btn btn-default']")
buttonchecked.click()
