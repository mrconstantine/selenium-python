from selenium import webdriver
import math

link = "http://suninjuly.github.io/math.html"
browser = webdriver.Chrome()
browser.get(link)


# Функция для расчета формулы на странице
def calc(x):
    return str(math.log(abs(12 * math.sin(int(x)))))


# Поиск формулы на странице
x_element = browser.find_element_by_xpath('//span[@id="input_value"]')
x = x_element.text
y = calc(x)

# Вставка ответа в поле ввода
input1 = browser.find_element_by_xpath('//input[@id="answer"]')
input1.send_keys(y)

# using checkbox
checkbox = browser.find_element_by_xpath("//label[@for='robotCheckbox']")
checkbox.click()

# using radiobutton
radiobutton = browser.find_element_by_xpath("//label[@for='robotsRule']")
radiobutton.click()

buttonchecked = browser.find_element_by_xpath("//button[@class='btn btn-default']")
buttonchecked.click()
